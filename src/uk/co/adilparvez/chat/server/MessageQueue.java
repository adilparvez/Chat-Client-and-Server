package uk.co.adilparvez.chat.server;

public class MessageQueue<T> {
	
	private static class Link<L> {
		L val;
		Link<L> next;
		
		Link(L val) {
			this.val = val;
			this.next = null;
		}
	}
	
	private Link<T> first = null;
	private Link<T> last = null;
	
	public synchronized void put(T msg) {
		Link<T> next = new Link<T>(msg);
		if (first == null) {
			first = next;
		} else {
			last.next = next;
			last = next;
		}
		this.notify();
	}

	public synchronized T take() {
		while (first == null) {
			try {
				this.wait();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		T taken = first.val;
		first = first.next;
		return taken;
	}

}