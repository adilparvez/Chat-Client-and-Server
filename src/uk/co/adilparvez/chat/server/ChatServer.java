package uk.co.adilparvez.chat.server;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;

import uk.co.adilparvez.chat.messagetype.Message;


public class ChatServer {
	
	private static int port;
	private static Database database;
	private static ServerSocket serverSocket;
	private static MultiQueue<Message> queues;
	
	private static final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("kk:mm:ss");
	
	public static void main(String[] args) {
		if (args.length != 2) {
			System.err.println("Arguments: <port> <database>");
			return;
		}
		
		try {
			port = Integer.parseInt(args[0]);
			if (port < 0 || port > 65535) {
				throw new NumberFormatException();
			}
			
			database = new Database(args[1]);
			serverSocket = new ServerSocket(port);
			queues = new MultiQueue<Message>();
			
			System.out.println(getDate() + " Server started.");
			
			while (true) {
				final Socket socket = serverSocket.accept();
				new Thread() {
					
					@Override
					public void run() {
						new ClientHandler(socket, queues, database);
					}
					
				}.start();
				
				System.out.println("............");
			}
			
		} catch (NumberFormatException e) {
			System.err.println("Arguments: <port> <database>");
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				if (database != null) {
					database.close();
				}
				if (serverSocket != null) {
					serverSocket.close();
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	private static String getDate() {
		return "[" + DATE_FORMAT.format(new Date()) + "]";
	}
	
}