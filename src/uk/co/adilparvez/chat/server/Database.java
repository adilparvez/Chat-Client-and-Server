package uk.co.adilparvez.chat.server;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import uk.co.adilparvez.chat.messagetype.RelayMessage;


public class Database {
	
	private Connection connection;
	
	public Database(String databasePath) throws SQLException {
		try {
			Class.forName("org.hsqldb.jdbcDriver");
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}

		connection = DriverManager.getConnection("jdbc:hsqldb:file:" + databasePath, "SA", "");
		
		Statement delayStmt = connection.createStatement();
		try {
			delayStmt.execute("SET WRITE_DELAY FALSE");
		} finally {
			delayStmt.close();
		}
		
		connection.setAutoCommit(false);
		
		Statement stmt1 = connection.createStatement();
		try {
			String messagesTable = "CREATE TABLE messages(nick VARCHAR(255) NOT NULL,"
					   + "message VARCHAR(4096) NOT NULL,"
					   + "timeposted BIGINT NOT NULL)";
			stmt1.execute(messagesTable);
		} finally {
			stmt1.close();
		}
		
		Statement stmt2 = connection.createStatement();
		try {
			String statisticsTable = "CREATE TABLE statistics(key VARCHAR(255), value INT)\n"
					   + "INSERT INTO statistics(key, value) VALUES ('Total messages', 0)\n"
					   + "INSERT INTO statistics(key, value) VALUES ('Total logins', 0)";
			stmt2.execute(statisticsTable);
		} finally {
			stmt2.close();
		}
		
		connection.commit();
	}
	
	public void close() throws SQLException {
		connection.close();
	}
	
	public synchronized void incrementLogins() throws SQLException {
		Statement stmt = connection.createStatement();
		try {
			stmt.execute("UPDATE statistics SET value = value + 1 WHERE key = 'Total logins'");
		} finally {
			stmt.close();
			connection.commit();
		}
	}
	
	public synchronized void addMessage(RelayMessage m) throws SQLException {
		PreparedStatement msgStmt = connection.prepareStatement("INSERT INTO messages(nick, message, timeposted) VALUES (?,?,?)");
		Statement statStmt = connection.createStatement();
		try {
			msgStmt.setString(1, m.getFrom());
			msgStmt.setString(2, m.getMessage());
			msgStmt.setString(3, Long.toString(m.getCreationTime().getTime()));
			msgStmt.executeUpdate();
			statStmt.execute("UPDATE statistics SET value = value + 1 WHERE key = 'Total messages'");
		} finally {
			msgStmt.close();
			statStmt.close();
			connection.commit();
		}
	}
	
	public synchronized List<RelayMessage> getRecent() throws SQLException {
		List<RelayMessage> messages = new ArrayList<RelayMessage>();
		PreparedStatement stmt = connection.prepareStatement("SELECT nick, message, timeposted FROM messages ORDER BY timeposted DESC LIMIT 10");
		try {
			ResultSet rs = stmt.executeQuery();
			try {
				while (rs.next()) {
					messages.add(new RelayMessage(rs.getString(1), rs.getString(2), new Date(rs.getLong(3))));
				}
			} finally {
				rs.close();
			}
		} finally {
			stmt.close();
		}
		return messages;
	}

}
