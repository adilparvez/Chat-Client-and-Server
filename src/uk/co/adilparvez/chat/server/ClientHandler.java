package uk.co.adilparvez.chat.server;

import java.io.EOFException;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.sql.SQLException;
import java.util.List;
import java.util.Random;

import uk.co.adilparvez.chat.messagetype.*;


public class ClientHandler {
	
	private final Socket socket;
	private final MultiQueue<Message> queues;
	private final Database database;
	
	private String nick;
	private final MessageQueue<Message> messages;
	
	private static Random rand = new Random();
	
	public ClientHandler(Socket sock, MultiQueue<Message> queue, Database database) {
		this.socket = sock;
		this.queues = queue;
		this.database = database;
		
		List<RelayMessage> recent = null;
	
		try {
			database.incrementLogins();
			recent = database.getRecent();
		} catch(SQLException e) {
			e.printStackTrace();
		}
		
		messages = new MessageQueue<Message>();
		
		if(recent != null) {
			for(int i = recent.size() - 1; i >= 0; i--) {
				messages.put(recent.get(i));
			}
		}
		
		queue.register(messages);
		
		nick = String.format("Anonymous%5d", rand.nextInt(100000));
		StatusMessage status = new StatusMessage(String.format("[%s] [%s] Connected.", nick, sock.getInetAddress().getHostName()));
		queue.put(status);
		
		Thread receiverThread = new Thread(new Receiver());
		receiverThread.start();
		
		Thread transmitterThread = new Thread(new Transmitter());
		transmitterThread.start();
		
		try {
			receiverThread.join();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
		try {
			socket.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
		
	private class Receiver implements Runnable {

		@Override
		public void run() {
			try {
				ObjectInputStream in = new ObjectInputStream(socket.getInputStream());
				while(true) {
					try {
						Message message = (Message) in.readObject();
						if(message instanceof ChangeNickMessage) {
							String old = nick;
							nick = ((ChangeNickMessage) message).getNick();
							queues.put(new StatusMessage(String.format("[%s] --> [%s].", old, nick)));
						} else if(message instanceof ChatMessage) {
							RelayMessage relay = new RelayMessage(nick,(ChatMessage) message);
							queues.put(relay);
							try {
								database.addMessage(relay);
							} catch (java.sql.SQLException e) {
								e.printStackTrace();
							}
						}
					} catch (ClassNotFoundException e) {
						e.printStackTrace();
					}
				}
			} catch (EOFException e) {
				queues.deregister(messages);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		
	}
	
	private class Transmitter implements Runnable {
		
		@Override
		public void run() {
			try {
				ObjectOutputStream out = new ObjectOutputStream(socket.getOutputStream());
				while(true) {
					out.writeObject(messages.take());
				}
			} catch (EOFException e) {
				queues.deregister(messages);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		
	}
	
}