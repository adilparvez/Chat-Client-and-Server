package uk.co.adilparvez.chat.client;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.text.SimpleDateFormat;
import java.util.Date;

import uk.co.adilparvez.chat.messagetype.*;

public class ChatClient {
	
	private static final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("kk:mm:ss");
	
	private static Socket socket;
	private static String server;
	private static int port;
	
	public static void main(String[] args) {
		if (args.length != 2) {
			System.err.println("Arguments: <server> <port>");
			return;
		}
		
		try {
			server = args[0];
			port = Integer.parseInt(args[1]);
			
			if (port < 0 || port > 65535) {
				throw new NumberFormatException();
			}
			
			socket = new Socket(server, port);
			
			System.out.println(getDate() + " Client connected to " + server + " on port " + port + ".");
			
			Thread receiverThread = new Thread(new Receiver());
			receiverThread.setDaemon(true);
			receiverThread.start();
			
			transmit();
			
		} catch (NumberFormatException e) {
			System.err.println("Arguments: <server> <port>");
			return;
		} catch (IOException e) {
			System.err.println("Cannot connect to " + server + " on port " + port + ".");
			return;
		} finally {
			try {
				socket.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
	
	private static String getDate() {
		return "[" + DATE_FORMAT.format(new Date()) + "]";
	}
	
	private static final class Receiver implements Runnable {

		@Override
		public void run() {
			ObjectInputStream in;
			
			try {
				in = new ObjectInputStream(socket.getInputStream());
				
				while (true) {
					Message message = (Message) in.readObject();
					
					if (message instanceof RelayMessage) {
						RelayMessage relayMessage = (RelayMessage) message;
						System.out.println(getDate() + " [" + relayMessage.getFrom() + "] " + relayMessage.getMessage());
					} else if (message instanceof StatusMessage) {
						StatusMessage statusMessage = (StatusMessage) message;
						System.out.println(getDate() + " [Server] " + statusMessage.getMessage());
					}
				}
				
			} catch (IOException e) {
				System.err.println("Cannot connect to " + server + " on port " + port + ".");
				return;
			} catch (ClassNotFoundException e) {
				e.printStackTrace();
				return;
			}
		}
		
	}
	
	public static void transmit() {
		ObjectOutputStream out;
		BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
		
		try {
			out = new ObjectOutputStream(socket.getOutputStream());
			
			while (true) {
				String message = reader.readLine();
				
				if (message.startsWith("\\")) {
					if (message.startsWith("\\nick ")) {
						String nick = message.substring(5).trim();
						out.writeObject(new ChangeNickMessage(nick));
					} else if (message.startsWith("\\quit")) {
						System.out.println(getDate() + " Connection terminated.");
						return;
					} else {
						System.out.println("Unknown command.");
					}
				} else {
					out.writeObject(new ChatMessage(message));
				}
			}
			
		} catch (IOException e) {
			System.err.println("Cannot connect to " + server + " on port " + port + ".");
			return;
		}
	}
}
