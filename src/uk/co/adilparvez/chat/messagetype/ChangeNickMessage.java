package uk.co.adilparvez.chat.messagetype;

import java.io.Serializable;

public class ChangeNickMessage extends Message implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private String nick;
	
	public ChangeNickMessage(String nick) {
		super();
		this.nick = nick;
	}
	
	public String getNick() {
		return nick;
	}
	
}
